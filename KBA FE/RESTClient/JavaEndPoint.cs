﻿namespace KBA_FE.RESTClient
{
    /**
     * Endpoint configuration
     * Reference these using JavaEndPoint.[Service].[Endpoint]
     *
     * They will later be translated into an URI using the rest client
     * ----- EXAMPLE -----
     * Given a Backend url of "http://localhost:8080", ENDPOINT1 = "/hello/world" will translate into http://localhost:8080/hello/world
     */

    public abstract class JavaEndPoint
    {
        // Spiellogik endpoints
        public abstract class Spiellogik
        {
            private const string ENDPOINT_PREFIX = "spiellogik";

            public static string SPIEL_STARTEN = $"{ENDPOINT_PREFIX}/spielStarten";
            public static string NEUE_RUNDE_STARTEN = $"{ENDPOINT_PREFIX}/neueRundeStarten";
            public static string NEUEN_SPIELZUG_STARTEN = $"{ENDPOINT_PREFIX}/neuenSpielzugStarten";
            public static string ZUG_MACHEN = $"{ENDPOINT_PREFIX}/zugMachen";
            public static string SPIEL_BEENDEN = $"{ENDPOINT_PREFIX}/spielBeenden";
        }

        // Nutzerverwaltung endpoints
        public abstract class Nutzerverwaltung
        {
            private const string ENDPOINT_PREFIX = "nutzerverwaltung";

            public static string SPIELER = $"{ENDPOINT_PREFIX}/spieler";

            public static string IST_SPIELER_REG = $"{ENDPOINT_PREFIX}/istSpielerRegistriert";
            public static string SPIELER_REG = $"{ENDPOINT_PREFIX}/spielerRegistrieren";
            public static string SPIELER_ANMELDEN = $"{ENDPOINT_PREFIX}/spielerAnmelden";
            public static string SPIELER_ABMELDEN = $"{ENDPOINT_PREFIX}/spielerAbmelden";
            public static string SPIELER_BEARBEITEN = $"{ENDPOINT_PREFIX}/spielerBearbeiten";
            public static string SPIELE_LISTE = $"{ENDPOINT_PREFIX}/getSpieleListe";
            public static string SPIELER_LOESCHEN = $"{ENDPOINT_PREFIX}/spielerLoeschen";
        }

        // Spielverwaltung endpoints
        public abstract class Spielverwaltung
        {
            private const string ENDPOINT_PREFIX = "spielverwaltung";

            public static string SPIEL_ANLEGEN = $"{ENDPOINT_PREFIX}/spielAnlegen";
            public static string OFFENES_SPIEL_SUCHEN = $"{ENDPOINT_PREFIX}/offenesSpielSuchen";
            public static string SPIEL_BEITRETEN = $"{ENDPOINT_PREFIX}/spielBeitreten";
            public static string SPIEL_VERLASSEN = $"{ENDPOINT_PREFIX}/spielVerlassen";
            public static string STATUS = $"{ENDPOINT_PREFIX}/getStatus";
        }

        // Unit endpoints
        public abstract class Unit
        {
            private const string ENDPOINT_PREFIX = "unit";

            public static string GET_UNITS = $"{ENDPOINT_PREFIX}/getUnits";
        }

        // Auswertung endpoints
        public abstract class Auswertung
        {
            private const string ENDPOINT_PREFIX = "auswertung";

            public static string SPIEL_AUSWERTEN = $"{ENDPOINT_PREFIX}/spielAuswerten";
            public static string GEWINNER_ERMITTELN = $"{ENDPOINT_PREFIX}/gewinnerErmitteln";
        }
    }
}