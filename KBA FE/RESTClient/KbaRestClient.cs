﻿using KBA_FE.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace KBA_FE.RESTClient
{
    public static class KbaRestClient
    {

        // Init Rest Client
        private static readonly HttpClient Client = new HttpClient();
        private const string JavaBackend = "http://localhost:8080";

        private static string UriBuilder(string endPoint)
        {
            return @$"{JavaBackend}/{endPoint}";
        }

        /* NUTZERVERWALTUNG */
        public static class Nutzerverwaltung
        {
            public static bool IstSpielerRegistriert(string name)
            {
                
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Nutzerverwaltung.IST_SPIELER_REG;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint) + $"?name={name}").Result;

                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<bool>(responseString);
            }

            public static SpielerModel SpielerRegistrieren(string name)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Nutzerverwaltung.SPIELER_REG;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint) + $"?name={name}").Result;

                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                var responseString = response.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<SpielerModel>(responseString);
            }

            public static SpielerModel SpielerAnmelden(string name)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Nutzerverwaltung.SPIELER_ANMELDEN;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint) + $"?name={name}").Result;

                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<SpielerModel>(responseString);
            }

            public static HttpStatusCode SpielerAbmelden(string name)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Nutzerverwaltung.SPIELER_ABMELDEN;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint) + $"?name={name}").Result;
                return response.StatusCode;
            }

            public static SpielerModel SpielerBearbeiten(int spielerId, string neuerName)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Nutzerverwaltung.SPIELER_BEARBEITEN;
                // -------------------------------------------------------------------------------------

                var response = Client.PostAsync(UriBuilder(endPoint),
                    new StringContent(JsonConvert.SerializeObject(new { spielerId, name = neuerName }), Encoding.UTF8, "application/json")).Result;

                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<SpielerModel>(responseString);
            }

            public static List<SpielModel> GetSpieleListe(int spielerId)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Nutzerverwaltung.SPIELE_LISTE;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint) + $"?spielerId={spielerId}").Result;

                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<List<SpielModel>>(responseString);
            }

            public static void SpielerLöschen(int spielerId)
            {
                throw new NotImplementedException();
            }
        }

        /* SPIELLOGIK  */
        public static class Spiellogik
        {
            

            public static SpielModel SpielStarten(int gameId)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Spiellogik.SPIEL_STARTEN;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint) + $"?spielId={gameId}").Result;

                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<SpielModel>(responseString);
            }

            public static RundeModel NeueRundeStarten(int gameId)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Spiellogik.NEUE_RUNDE_STARTEN;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint) + $"?spielId={gameId}").Result;
                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<RundeModel>(responseString);
            }

            public static SpielzugModel NeuenSpielzugStarten(int gameId)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Spiellogik.NEUEN_SPIELZUG_STARTEN;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint) + $"?spielId={gameId}").Result;
                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<SpielzugModel>(responseString);
            }

            public static HttpStatusCode ZugMachen(string userName, int gameId, int vocabId)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Spiellogik.ZUG_MACHEN;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint) + $"?spielId={gameId}").Result;
                return response.StatusCode;
            }

            public static HttpStatusCode SpielBeenden(int gameId)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Spiellogik.SPIEL_BEENDEN;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint) + $"?spielId={gameId}").Result;
                return response.StatusCode;
            }
        }

        public class SpielVerwaltung
        {
            public static SpielModel SpielAnlegen(int unitId, int spielerId)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Spielverwaltung.SPIEL_ANLEGEN;
                // -------------------------------------------------------------------------------------


                var response = Client.PostAsync(UriBuilder(endPoint),
                    new StringContent(JsonConvert.SerializeObject(new { unitId, spielerId }), Encoding.UTF8, "application/json")).Result;

                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<SpielModel>(responseString);
            }

            public static SpielModel SpielBeitreten(int spielId, int spielerId)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Spielverwaltung.SPIEL_BEITRETEN;
                // -------------------------------------------------------------------------------------


                var response = Client.PostAsync(UriBuilder(endPoint),
                    new StringContent(JsonConvert.SerializeObject(new { spielId, spielerId }), Encoding.UTF8, "application/json")).Result;

                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<SpielModel>(responseString);
            }

            public static HttpStatusCode SpielVerlassen(int spielId, int spielerId)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Spielverwaltung.SPIEL_VERLASSEN;
                // -------------------------------------------------------------------------------------

                var response = Client.PostAsync(UriBuilder(endPoint),
                    new StringContent(JsonConvert.SerializeObject(new { spielId, spielerId }), Encoding.UTF8, "application/json")).Result;

                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                return response.StatusCode;
            }

            public static SpielModel OffenesSpielSuchen(int unitId, int spielerId)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Spielverwaltung.OFFENES_SPIEL_SUCHEN;
                // -------------------------------------------------------------------------------------

                var response = Client.PostAsync(UriBuilder(endPoint),
                    new StringContent(JsonConvert.SerializeObject(new { unitId, spielerId }), Encoding.UTF8, "application/json")).Result;

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<SpielModel>(responseString);
            }

            public static SpielModel GetStatus(int spielId)
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Spielverwaltung.STATUS;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint) + $"?spielId={spielId}").Result;

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<SpielModel>(responseString);
            }
        }


        /* Units */
        public class Units
        {
            public static List<UnitModel> GetUnits()
            {
                // Change endpoint accordingly ---------------------------------------------------------
                var endPoint = JavaEndPoint.Unit.GET_UNITS;
                // -------------------------------------------------------------------------------------

                var response = Client.GetAsync(UriBuilder(endPoint)).Result;

                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();

                var responseString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<List<UnitModel>>(responseString);
            }
        }
    }
}