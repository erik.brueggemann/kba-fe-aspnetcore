﻿namespace KBA_FE.Models
{
    public class GameLeaveModel
    {
        public int GameId { get; set; }
        public int UserId { get; set; }

        public GameLeaveModel(int gameId, int userId)
        {
            GameId = gameId;
            UserId = userId;
        }
    }
}