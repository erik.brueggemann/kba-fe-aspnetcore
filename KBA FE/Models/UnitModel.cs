﻿using System.Collections.Generic;

namespace KBA_FE.Models
{
    public class UnitModel
    {
        public int Id { get; set; }
        public int KapitelNr { get; set; }
        public string Beschreibung { get; set; }
        public SprachrichtungModel Sprachrichtung { get; set; }
        public List<VokabelModel> Vokabeln { get; set; }

        public UnitModel()
        {
        }

        public UnitModel(int id, int kapitelNr, string beschreibung, SprachrichtungModel sprachrichtung, List<VokabelModel> vokabeln)
        {
            Id = id;
            KapitelNr = kapitelNr;
            Beschreibung = beschreibung;
            Sprachrichtung = sprachrichtung;
            Vokabeln = vokabeln;
        }
    }
}