﻿using System.Collections.Generic;

namespace KBA_FE.Models
{
    public class VokabelModel
    {
        public int VokabelId { get; set; }
        public SpracheModel Sprache { get; set; }
        public string Vokabel { get; set; }
        public HashSet<VokabelModel> Uebersetzungen { get; set; }
        public VokabelModel Synonym { get; set; }

        public VokabelModel()
        {
        }

        public VokabelModel(int vokabelId, SpracheModel sprache, string vokabel, HashSet<VokabelModel> uebersetzungen, VokabelModel synonym)
        {
            VokabelId = vokabelId;
            Sprache = sprache;
            Vokabel = vokabel;
            Uebersetzungen = uebersetzungen;
            Synonym = synonym;
        }
    }
}