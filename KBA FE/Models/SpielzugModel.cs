﻿using System.Collections.Generic;

namespace KBA_FE.Models
{
    public class SpielzugModel
    {
        public int SpielzugId { get; set; }
        public int SpielzugNummer { get; set; }
        public VokabelModel VokabelModel { get; set; }
        public VokabelModel RichtigeAntwort { get; set; }
        public List<VokabelModel> AntwortMoeglichkeiten { get; set; }
        public bool IstBeendet { get; set; }
        public VokabelModel AntwortSpieler1 { get; set; }
        public VokabelModel AntwortSpieler2 { get; set; }

        public SpielzugModel()
        {
        }

        public SpielzugModel(int spielzugId, int spielzugNummer, VokabelModel vokabelModel, VokabelModel richtigeAntwort, List<VokabelModel> antwortMoeglichkeiten, bool istBeendet, VokabelModel antwortSpieler1, VokabelModel antwortSpieler2)
        {
            SpielzugId = spielzugId;
            SpielzugNummer = spielzugNummer;
            VokabelModel = vokabelModel;
            RichtigeAntwort = richtigeAntwort;
            AntwortMoeglichkeiten = antwortMoeglichkeiten;
            IstBeendet = istBeendet;
            AntwortSpieler1 = antwortSpieler1;
            AntwortSpieler2 = antwortSpieler2;
        }
    }
}