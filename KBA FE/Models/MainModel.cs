﻿using System.Collections.Generic;

namespace KBA_FE.Models
{
    public class MainModel
    {
        public SpielerModel User { get; set; }
        public List<SpielModel> Games { get; set; }
        public List<UnitModel> Units { get; set; }

        public MainModel(SpielerModel user, List<SpielModel> games, List<UnitModel> units)
        {
            User = user;
            Games = games;
            Units = units;
        }
    }
}