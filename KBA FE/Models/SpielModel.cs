﻿using System.Collections.Generic;

namespace KBA_FE.Models
{
    public class SpielModel
    {
        public int Id { get; set; }
        public SpielerModel Spieler1 { get; set; }
        public SpielerModel Spieler2 { get; set; }
        public SpielerModel SpielerAmZug { get; set; }
        public UnitModel Unit { get; set; }
        public List<RundeModel> Runden { get; set; }
        public RundeModel AktuelleRunde { get; set; }
        public bool IstGestartet { get; set; }
        public bool IstBeendet { get; set; }
        public int RichtigeFragenSpieler1 { get; set; }
        public int RichtigeFragenSpieler2 { get; set; }

        public SpielModel()
        {
        }

        public SpielModel(int id, SpielerModel spieler1, SpielerModel spieler2, SpielerModel spielerAmZug, UnitModel unit, List<RundeModel> runden, RundeModel aktuelleRunde, bool istGestartet, bool istBeendet, int richtigeFragenSpieler1, int richtigeFragenSpieler2)
        {
            this.Id = id;
            this.Spieler1 = spieler1;
            this.Spieler2 = spieler2;
            this.SpielerAmZug = spielerAmZug;
            this.Unit = unit;
            this.Runden = runden;
            this.AktuelleRunde = aktuelleRunde;
            this.IstGestartet = istGestartet;
            this.IstBeendet = istBeendet;
            this.RichtigeFragenSpieler1 = richtigeFragenSpieler1;
            this.RichtigeFragenSpieler2 = richtigeFragenSpieler2;
        }
    }
}