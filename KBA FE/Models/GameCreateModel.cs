﻿using System.Collections.Generic;

namespace KBA_FE.Models
{
    public class GameCreateModel
    {
        public int UserId { get; set; }
        public List<UnitModel> Units { get; set; }

        public GameCreateModel(int userId, List<UnitModel> units)
        {
            UserId = userId;
            Units = units;
        }
    }
}