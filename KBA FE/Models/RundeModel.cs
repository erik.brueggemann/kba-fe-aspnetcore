﻿using System.Collections.Generic;

namespace KBA_FE.Models
{
    public class RundeModel
    {
        public int RundeId { get; set; }
        public Rundennummer Nummer { get; set; }
        public bool IstBeendet { get; set; }
        public List<SpielzugModel> Spielzuege { get; set; }
        public SpielzugModel AktuellerSpielzug { get; set; }
        public SpielModel Spiel { get; set; }
        public SpielerModel Beginner { get; set; }

        public RundeModel()
        {
        }

        public RundeModel(int rundeId, Rundennummer nummer, bool istBeendet, List<SpielzugModel> spielzuege, SpielzugModel aktuellerSpielzug, SpielModel spiel, SpielerModel beginner)
        {
            RundeId = rundeId;
            Nummer = nummer;
            IstBeendet = istBeendet;
            Spielzuege = spielzuege;
            AktuellerSpielzug = aktuellerSpielzug;
            Spiel = spiel;
            Beginner = beginner;
        }
    }
}