﻿namespace KBA_FE.Models
{
    public enum Rundennummer
    {
        RUNDE_1,
        RUNDE_2,
        RUNDE_3,
        RUNDE_4,
        RUNDE_5,
        RUNDE_6
    }
}