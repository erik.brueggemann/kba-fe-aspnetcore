﻿namespace KBA_FE.Models
{
    public class SpracheModel
    {
        public int SpracheId { get; set; }
        public string Name { get; set; }

        public SpracheModel()
        {
        }

        public SpracheModel(int spracheId, string name)
        {
            SpracheId = spracheId;
            Name = name;
        }
    }
}