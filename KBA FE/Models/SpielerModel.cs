﻿namespace KBA_FE.Models
{
    public class SpielerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IstAngemeldet { get; set; }
        public double GewinnProzentsatz { get; set; }
        public int GewonneneSpiele { get; set; }
        public int VerloreneSpiele { get; set; }
        public int UnentschiedeneSpiele { get; set; }

        public SpielerModel()
        {
        }

        public SpielerModel(int id, string name, bool istAngemeldet, double gewinnProzentsatz, int gewonneneSpiele, int verloreneSpiele, int unentschiedeneSpiele)
        {
            Id = id;
            Name = name;
            IstAngemeldet = istAngemeldet;
            GewinnProzentsatz = gewinnProzentsatz;
            GewonneneSpiele = gewonneneSpiele;
            VerloreneSpiele = verloreneSpiele;
            UnentschiedeneSpiele = unentschiedeneSpiele;
        }
    }
}