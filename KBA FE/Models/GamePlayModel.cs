﻿namespace KBA_FE.Models
{
    public class GamePlayModel
    {
        public int UserId { get; set; }
        public SpielModel Spiel { get; set; }

        public GamePlayModel(int userId, SpielModel spiel)
        {
            UserId = userId;
            Spiel = spiel;
        }
    }
}