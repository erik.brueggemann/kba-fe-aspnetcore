﻿namespace KBA_FE.Models
{
    public class SprachrichtungModel
    {
        public int SprachrichtungId { get; set; }
        public SpracheModel VonSprache { get; set; }
        public SpracheModel NachSprache { get; set; }

        public SprachrichtungModel()
        {
        }

        public SprachrichtungModel(int sprachrichtungId, SpracheModel vonSprache, SpracheModel nachSprache)
        {
            SprachrichtungId = sprachrichtungId;
            VonSprache = vonSprache;
            NachSprache = nachSprache;
        }
    }
}