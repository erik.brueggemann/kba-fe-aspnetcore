﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.
// Write your JavaScript code.

/* GENERAL STUFF */
/* -------------------------------------- */
const refresh = () => {
    location.reload();
}

$('[data-toggle="tooltip"]').tooltip();

/* VALIDATION */
/* -------------------------------------- */
const validate_user = (userName) => {
    if (userName.length <= 3) {
        return false;
    }

    if (userName.length >= 15) {
        return false;
    }

    return true;
}

/* USER SIGN IN, UP AND OUT */
/* -------------------------------------- */
const sign_in = () => {
    $("#action-alert").hide();
    $("#confirm-alert").hide();
    $("#error-alert").hide();

    const userName = $("#user-input").val();

    if (validate_user(userName)) {
        $.ajax({
            type: "POST",
            url: "/home/SignIn",
            data: {
                UserName: userName
            },
            beforeSend: function () {
                $("#action-text").text("Du wirst angemeldet ...");
                $("#action-alert").show();
            },
            complete: function () {
                $("#action-alert").hide();
            },
            success: response => {
                if (response.result === "Redirect") {
                    window.location = `${response.url}?username=${response.userName}`;
                } else {
                    $("#error-text").text(response.msg);
                    $("#error-alert").show();
                }
            }
        });
    } else {
        $("#error-text").text("Bitte gib ein gültigen Nutzernamen ein!");
        $("#error-alert").show();
    }
};

const sign_up = () => {
    $("#action-alert").hide();
    $("#confirm-alert").hide();
    $("#error-alert").hide();

    const userName = $("#user-input").val();

    if (validate_user(userName)) {
        $.ajax({
            type: "POST",
            url: "/home/SignUp",
            data: {
                UserName: userName
            },
            beforeSend: function () {
                $("#action-text").text("Wir erstellen dein Nutzer ...");
                $("#action-alert").show();
            },
            complete: function () {
                $("#action-alert").hide();
            },
            success: response => {
                if (response.result === "Success") {
                    $("#confirm-text").text(`Willkommen ${response.userName}! Viel Spaß.`);
                    $("#confirm-alert").show();
                } else {
                    $("#error-text").text(response.msg);
                    $("#error-alert").show();
                }
            },
            error: response => {
                $("#error-text").text(response.msg);
                $("#error-alert").show();
            }
        });
    } else {
        $("#error-text").text("Bitte wähle einen gültigen Nutzernamen!");
        $("#error-alert").show();
    }
};

function sign_out(userName) {
    $.ajax({
        type: "POST",
        url: "/User/SignOut",
        data: {
            UserName: userName
        },
        success: response => {
            if (response.result === 'Redirect') {
                window.location = `${response.url}`;
            } else {
                alert(response.msg);
            }
        },
        error: response => {
            alert("Controller call fehlgeschlagen");
        }
    });
}

/* ENTER GAME */
/* -------------------------------------- */
function enter_modal(userId, unitId) {

    // prepare modal
    $("#game-enter-modal-content").html('     <div class="text-center p-5">\r\n        <div id="load-spinner" class="loader mb-3" style="height: 128px; width: 128px; margin: auto;"></div>\r\n        <h4>Suche nach einem Spiel ...</h4>\r\n        </div>');
    $("#game-enter-modal").modal("show");

    $.ajax({
        type: "POST",
        url: "/Game/EnterModal",
        data: {
            UserId: userId,
            UnitId: unitId
        },
        success: response => {
            $("#game-enter-modal-content").html(response);
        },
        error: response => {
            alert("[DEMO] Controller call fehlgeschlagen");
        }
    });
}

function enter_game(gameId, userId) {
    $.ajax({
        type: "POST",
        url: "/Game/EnterGame",
        data: {
            GameId: gameId,
            UserId: userId
        },
        beforeSend: function () {
            $("#load-spinner").show();
        },
        complete: function () {
            $("#load-spinner").hide();
        },
        success: response => {
            if (response.result === "Success") {
                $("#game-enter-modal").modal("hide");
                location.reload();
            } else {
                alert("Controller call fehlgeschlagen.");
            }
        }
    });
}

/* VIEW GAME */
/* -------------------------------------- */
function view_modal(userId, gameId) {
    $.ajax({
        type: "POST",
        url: "/Game/ViewModal",
        data: {
            UserId: userId,
            GameId: gameId
        },
        success: response => {
            $("#game-view-modal-content").html(response);
            $("#game-view-modal").modal("show");
        },
        error: response => {
            alert("[DEMO] Controller call fehlgeschlagen");
        }
    });
}

/* PLAY GAME */
/* -------------------------------------- */

function play_modal(userId, gameId) {
    $.ajax({
        type: "POST",
        url: "/Game/PlayModal",
        data: {
            UserId: userId,
            GameId: gameId
        },
        success: response => {
            $("#game-play-modal-content").html(response);
            $("#game-play-modal").modal("show");
        },
        error: response => {
            alert("[DEMO] Controller call fehlgeschlagen");
        }
    });
}

function play_move(userId, gameId) {
    $.ajax({
        type: "POST",
        url: "/Game/PlayMove",
        data: {
            UserId: userId,
            GameId: gameId
        },
        success: response => {
            $("#game-move-content").html(response);
            start_move_timer(userName, gameId);
        },
        error: response => {
            alert("[DEMO] Controller call fehlgeschlagen");
        }
    });
}

const time = 200000;
let auto_submit_timer;

function auto_submit(userName, gameId) {
    auto_submit_timer = setTimeout(function () {
        submit_answer(userName, gameId);
    }, time);
};

function start_move_timer(userName, gameId) {
    $("#move-timer").animate({
        width: "100%"
    }, time);
    auto_submit(userName, gameId);
}

function stop_move_timer() {
    $("#move-timer").stop();
}

function submit_answer(userId, gameId) {
    $.ajax({
        type: "POST",
        url: "/Game/PlayMoveSubmit",
        data: {
            UserId: userId,
            GameId: gameId,
            SelectedAnswer: $("label.active").attr("id")
        },
        beforeSend: function () {
            stop_move_timer();
            clearTimeout(auto_submit_timer);
        },
        complete: function () {
            // not sure yet
        },
        success: response => {
            if (response.result === "NextMove") {
                // Trigger the next move
                alert(response.message);
                play_move(userName, gameId);
            } else {
                // That was the last move
                clearTimeout(auto_submit_timer);
                alert(response.message);
                $("#game-play-modal").modal("hide");
                location.reload();
            }
        },
        error: response => {
            alert("[DEMO] Controller call fehlgeschlagen");
        }
    });
}

/* CREATE GAME */
/* -------------------------------------- */
function create_modal(userId) {
    $.ajax({
        type: "POST",
        url: "/Game/CreateModal",
        data: {
            UserId: userId
        },
        success: response => {
            $("#game-create-modal-content").html(response);
            $("#game-create-modal").modal("show");
        },
        error: response => {
            alert("[DEMO] Controller call fehlgeschlagen");
        }
    });
}

function create_game(userId, unitId) {
    $.ajax({
        type: "POST",
        url: "/Game/CreateGame",
        data: {
            UserId: userId,
            UnitId: unitId
        },
        beforeSend: function () {
            $("#load-spinner").show();
        },
        complete: function () {
            $("#load-spinner").hide();
        },
        success: response => {
            if (response.result === "Success") {
                $("#game-enter-modal").modal("hide");
                location.reload();
            } else {
                alert("[DEMO] Controller call fehlgeschlagen");
            }
        },
        error: response => {
            alert("[DEMO] Controller call fehlgeschlagen");
        }
    });
}

/* DELETE GAME */
/* -------------------------------------- */
function leave_modal(gameId, userId) {
    $.ajax({
        type: "POST",
        url: "/Game/LeaveModal",
        data: {
            GameId: gameId,
            UserId: userId
        },
        success: response => {
            $("#game-create-modal-content").html(response);
            $("#game-create-modal").modal("show");
        },
        error: response => {
            alert("[DEMO] Controller call fehlgeschlagen");
        }
    });
}

function leave_game(gameId, userId) {
    $.ajax({
        type: "POST",
        url: "/Game/LeaveGame",
        data: {
            GameId: gameId,
            UserId: userId
        },
        beforeSend: function () {
            $("#load-spinner").show();
        },
        complete: function () {
            $("#load-spinner").hide();
        },
        success: response => {
            if (response.result === "Success") {
                $("#game-leave-modal").modal("hide");
                location.reload();
            } else {
                alert("[DEMO] Controller call fehlgeschlagen");
            }
        },
        error: response => {
            alert("[DEMO] Controller call fehlgeschlagen");
        }
    });
}