﻿using KBA_FE.Models;
using KBA_FE.RESTClient;
using Microsoft.AspNetCore.Mvc;
using System;

namespace KBA_FE.Controllers
{
    public class UserController : Controller
    {
        public IActionResult ChangeNameModal(SpielerModel spieler)
        {
            return PartialView("~/Views/User/_UserChangeNameModalContent.cshtml", spieler);
        }

        public JsonResult ChangeName(string userName, int userId, string oldName, string newName)
        {
            if (oldName != userName)
            {
                return Json(new { result = "Error", msg = "Dein aktueller Name stimmt nicht überein!" });
            }

            try
            {
                // Spieler ändern
                KbaRestClient.Nutzerverwaltung.SpielerBearbeiten(userId, newName);
                //Spieler mit neuem Namen ausloggen
                KbaRestClient.Nutzerverwaltung.SpielerAbmelden(newName);
            }
            catch (Exception e)
            {
                return Json(new { result = "Error", msg = e.Message });
            }
            

            // Redirect to index aka log user out
            return Json(new { result = "Redirect", url = Url.Action("Index", "Home") });
        }

        public JsonResult SignOut(string userName)
        {
            // Send user state to java backend
            try
            {
                KbaRestClient.Nutzerverwaltung.SpielerAbmelden(userName);
            } catch (Exception e)
            {
                return Json(new { result = "Error", msg = e.Message });

            }
            

            // Redirect to index
            return Json(new { result = "Redirect", url = Url.Action("Index", "Home") });
        }
    }
}