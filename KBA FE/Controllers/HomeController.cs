﻿using KBA_FE.Models;
using KBA_FE.RESTClient;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace KBA_FE.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public JsonResult SignIn(string userName)
        {
            // JAVA BACKEND LOGIC

            var registered = false;
            try
            {
                registered = KbaRestClient.Nutzerverwaltung.IstSpielerRegistriert(userName);
            }
            catch (Exception)
            {
                Json(new { result = "Error", msg = "Du bist bei uns nicht registiert!" });
            }

            return registered == false ?
                Json(new { result = "Error", msg = "Du bist bei uns nicht registiert!" }) :
                Json(new { result = "Redirect", url = Url.Action("Index", "Game"), userName });
        }

        public JsonResult SignUp(string userName)
        {
            // JAVA BACKEND LOGIC
            if (KbaRestClient.Nutzerverwaltung.IstSpielerRegistriert(userName))
                return Json(new { result = "Error", msg = "Du bist bereits registriert!" });

            try
            {
                KbaRestClient.Nutzerverwaltung.SpielerRegistrieren(userName);
                return Json(new { result = "Success", userName });
            }
            catch (Exception e)
            {
                return Json(new { result = "Error", msg = e.Message });
            }

        }

    }
}