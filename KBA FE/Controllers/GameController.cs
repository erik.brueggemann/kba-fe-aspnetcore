﻿using KBA_FE.Models;
using KBA_FE.RESTClient;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading;

namespace KBA_FE.Controllers
{
    public class GameController : Controller
    {
        public IActionResult Index(string username)
        {
            // GET data from java backend with username
            SpielerModel spieler = null;
            try
            {
                spieler = KbaRestClient.Nutzerverwaltung.SpielerAnmelden(username);
            } catch (Exception e)
            {
                // Logout vergessen ...
                KbaRestClient.Nutzerverwaltung.SpielerAbmelden(username);
                spieler = KbaRestClient.Nutzerverwaltung.SpielerAnmelden(username);
            }

            // GET data from java backend for active games
            var activeGames = KbaRestClient.Nutzerverwaltung.GetSpieleListe(spieler.Id);

            // GET data from java backend for all units
            var units = KbaRestClient.Units.GetUnits();

            // Create main model
            var mainModel = new MainModel(
                spieler,
                activeGames,
                units
                );

            return View(mainModel);
        }

        /* ENTER GAME */
        /* -------------------------------------- */
        [HttpPost]
        public IActionResult EnterModal(int userId, int unitId)
        {
            var offenesSpiel = KbaRestClient.SpielVerwaltung.OffenesSpielSuchen(unitId, userId);
            Thread.Sleep(250);

            if (offenesSpiel == null)
            {
                return PartialView("~/Views/Game/_GameEnterModalErrorContent.cshtml");
            }

            var model = new GamePlayModel(userId, offenesSpiel);
            return PartialView("~/Views/Game/_GameEnterModalContent.cshtml", model);

        }

        [HttpPost]
        public JsonResult EnterGame(int gameId, int userId)
        {
            var spiel = KbaRestClient.SpielVerwaltung.SpielBeitreten(gameId, userId);

            if (spiel == null)
            {
                return Json(new { result = "Error" });
            }

            return Json(new { result = "Success" });
        }

        /* VIEW GAME */
        /* -------------------------------------- */

        [HttpPost]
        public IActionResult ViewModal(int userId, int gameId)
        {
            // Spielstatus abfragen
            var spiel = KbaRestClient.SpielVerwaltung.GetStatus(gameId);

            var model = new GamePlayModel(userId, spiel);
            return PartialView("~/Views/Game/_GameViewModalContent.cshtml", model);
        }

        /* PLAY GAME */
        /* -------------------------------------- */

        [HttpPost]
        public IActionResult PlayModal(int userId, int gameId)
        {
            // Spielstatus abfragen
            var spiel = KbaRestClient.SpielVerwaltung.GetStatus(gameId);

            var model = new GamePlayModel(userId, spiel);
            return PartialView("~/Views/Game/_GamePlayModalContent.cshtml", model);
        }

        [HttpPost]
        public IActionResult PlayMove(int userId, int gameId)
        {
            var bla = KbaRestClient.Spiellogik.NeuenSpielzugStarten(gameId);

            // Spielstatus abfragen
            var spiel = KbaRestClient.SpielVerwaltung.GetStatus(gameId);

            var model = new GamePlayModel(userId, spiel);

            return PartialView("~/Views/Game/_GameMove.cshtml", model);
        }

        [HttpPost]
        public JsonResult PlayMoveSubmit(string userName, int gameId, int selectedAnswer)
        {
            /*
             _____________________________________________________________________________
             DEMO Logik
             _____________________________________________________________________________
            // SEND userName, gameId and selected answer to backend for processing
            var currentGame = Demo.SPIELE.Find(model => model.Id == gameId);
            var currenRound = currentGame.AktuelleRunde;
            var currentMove = currenRound.AktuellerSpielzug;

            bool nextMovePossible;

            try
            {
                Demo.SPIELE[gameId - 1].Runden[(int)currenRound.Nummer].AktuellerSpielzug.IstBeendet = true;
                Demo.SPIELE[gameId - 1].Runden[(int)currenRound.Nummer].AktuellerSpielzug = Demo.SPIELE[gameId - 1]
                    .Runden[(int)currenRound.Nummer].Spielzuege[currentMove.SpielzugId];

                nextMovePossible = true;
            }
            catch
            {
                Demo.SPIELE[gameId - 1].Runden[(int)currenRound.Nummer].AktuellerSpielzug.IstBeendet = true;
                Demo.SPIELE[gameId - 1].SpielerAmZug = Demo.IVAN;
                nextMovePossible = false;
            }
            */

            // Die Spielzug-Route im JAVA Backend funktioniert leider aus einem unerklärlichen Grund nicht :(
            // Wir haben mehrere Tage ohne Erfolg nach dem Fehler gesucht. 

            // Alle anderen Funktionalitäten im BE und FE funktionieren jedoch
            if (true)
            {
                return Json(new { result = "NextMove", message = $"Submitted UserName {userName} GameId {gameId} and SelectedAnswer {selectedAnswer} to backend. Next move possible!" });
            }
            else
            {
                return Json(new { result = "LastMove", message = $"Submitted UserName {userName} GameId {gameId} and SelectedAnswer {selectedAnswer} to backend. That was the last move!" });
            }
        }

        /* CREATE GAME */
        /* -------------------------------------- */

        [HttpPost]
        public IActionResult CreateModal(int userId)
        {
            // GET data from java backend
            var units = KbaRestClient.Units.GetUnits();
            var model = new GameCreateModel(userId, units);

            return PartialView("~/Views/Game/_GameCreateModalContent.cshtml", model);
        }

        [HttpPost]
        public JsonResult CreateGame(int userId, int unitId)
        {
            KbaRestClient.SpielVerwaltung.SpielAnlegen(unitId, userId);
            return Json(new { result = "Success", message = "" });
        }

        /* LEAVE GAME */
        /* -------------------------------------- */

        [HttpPost]
        public IActionResult LeaveModal(int gameId, int userId)
        {
            // GET data from java backend
            var units = KbaRestClient.Units.GetUnits();

            var model = new GameLeaveModel(gameId, userId);

            return PartialView("~/Views/Game/_GameLeaveModalContent.cshtml", model);
        }

        [HttpPost]
        public JsonResult LeaveGame(int gameId, int userId)
        {
            KbaRestClient.SpielVerwaltung.SpielVerlassen(gameId, userId);
            return Json(new { result = "Success", message = "" });
        }
    }
}